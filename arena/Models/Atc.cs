using Arena.ENums;

namespace Arena.Models
{
    public class Atc
    {
        public string name { get; set; }
        public AttTp attTp { get; set; }
        public int dmg { get; set; }
    }
}