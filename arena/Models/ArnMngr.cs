using Arena.GlobalState;

namespace Arena.Models
{
    public class ArnMngr
    {
        private Arn _arn;
        public ArnMngr()
        {
            _arn = Arn.Instance;
        }

        public void start()
        {
            _arn.start();
        }
    }
}