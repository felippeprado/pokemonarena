using Arena.Abstract;
using Arena.ENums;

namespace Arena.Models
{
    public class Pkn : aPkn
    {
        public int cChc { get; set; }
        public int sp { get; set; }
        public int spA { get; set; }
        public int acc { get; set; }

        public override PknTp pknTp => PknTp.FIRE;
    }

    public class PknNm : aPkn
    {
        public int cChc { get; set; }
        public int sp { get; set; }
        public int spA { get; set; }
        public int acc { get; set; }
        public override PknTp pknTp => PknTp.NM;
    }

    public class PknWtr : aPkn
    {
        public int cChc { get; set; }
        public int sp { get; set; }
        public int acc { get; set; }
        public override PknTp pknTp => PknTp.WTR;
    }
}