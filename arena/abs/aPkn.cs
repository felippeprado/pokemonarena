using System.Collections.Generic;
using Arena.ENums;
using Arena.GlobalState;
using Arena.Models;

namespace Arena.Abstract
{
    public abstract class aPkn
    {
        private DmgCalc _dmgCalc;
        public aPkn()
        {
            _dmgCalc = DmgCalc.Instance;
        }
        public string name { get; set; }
        public int hPts { get; set; }
        public int aPts { get; set; }
        public bool ftd { get; set; }
        public List<Atc> atcList { get; set; }
        public abstract PknTp pknTp { get; }

        public int tkdmg(int dmg, AttTp attTp)
        {
            //#4 DESAFIO
            // UTILIZAR O RESULTADO DA CALCULADORA PARA SUBTRAIR O DANO DOS HITPOINTS

            //#6 DESAFIO
            // SE OS HITPOINTS FOREM MENOR QUE 0, UTILIZAR FLAG PARA INDICAR QUE O POKEMON DESMAIOU ZZZZZ

            //#7 DESAFIO
            // RETORNAR O DANO CALCULADO
            return 0;
        }
    }
}