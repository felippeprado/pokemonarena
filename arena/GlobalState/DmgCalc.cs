﻿using Arena.ENums;

namespace Arena.GlobalState
{
    //NÃO MEXER
    public class DmgCalc
    {
        private static DmgCalc instance = null;

        public int calcDmg(AttTp attTp, PknTp pknTp, int damage)
        {
            var x = damage;

            //#7 DESAFIO
            // BASEADO NO TIPO DE ATAQUE E DO POKEMON QUE ESTA RECEBENDO O DANO, UTILIZAR 
            // https://pkmn.help/
            // PARA VERIFICAR SE HA MODIFICADORES

            return x;
        }

        private DmgCalc()
        {
        }

        public static DmgCalc Instance
        {
            get
            {
                DmgCalc _instance;
                if (instance == null)
                {
                    _instance = new DmgCalc();
                    return _instance;
                }
                else
                {
                    return Instance;
                }
            }
        }
    }
}
