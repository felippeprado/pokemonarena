﻿namespace Arena.ENums
{
    public enum AttTp
    {
        FIRE,
        WTR,
        PHSCAL,
        ICE,
        GRD,
        DRG,
        RCK,
        FGHT,
        GHT,
        NM,
        DRK
    }
}

