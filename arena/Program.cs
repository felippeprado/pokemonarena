﻿using System;
using Arena.Models;

namespace Arena
{
    class Program
    {
        static void Main(string[] args)
        {
            var seed = new Seed.Seed();
            seed.start();

            var arena = new ArnMngr();
            arena.start();
        }
    }
}
